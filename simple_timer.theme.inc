<?php
/**
 * @file
 * Theme functions for the simple timer.
 */

/**
 * Implements template_preprocess_simple_timer_timer().
 */
function template_preprocess_simple_timer_timer(&$vars) {
  $path = drupal_get_path('module', 'simple_timer');
  drupal_add_js($path.'/libraries/timer.jquery/src/timer.jquery.js');
  drupal_add_js($path.'/js/simple-timer.js');

  $element = $vars['simple_timer'];
  $vars['timer_id'] = $element['#timer']->id;
  $vars['timer_key'] = $element['#timer']->timer_key;
  $vars['timer_category'] = $element['#timer']->category;
  $vars['timer_behavior'] = !empty($element['#behavior']) ? $element['#behavior'] : 'show';

  if ($vars['timer_behavior'] == 'auto_alert' && ($element['#timer']->status == 'paused')) {
    // Look for a running timer in the same category.
    $conflicting = entity_get_controller('simple_timer')->getConflicting($element['#timer']);

    if (empty($conflicting)) {
      $element['#timer']->unpause();
    }
    else {
      // Show an alert.
      $vars['classes_array'][] = 'simple-timer-timer-show-unpause-alert';
    }
  }

  $vars['initial_seconds'] = $element['#timer']->duration();
  $vars['initial_status'] = $element['#timer']->status;

  // Buttons.
  $vars['show_stop'] = TRUE;
  $vars['show_pause'] = TRUE;
  $vars['show_resume'] = TRUE;
  $vars['show_cancel'] = TRUE;

  // Button Texts.
  $vars['stop_button_text'] = '&#10004';
  $vars['pause_button_text'] = '&#10073&#10073';
  $vars['resume_button_text'] = '&#9658';
  $vars['cancel_button_text'] = '&#10006';

  $id = 'timer-id-'.$element['#timer']->id;
  $vars['id'] = drupal_html_id($id);
  $vars['classes_array'][] = 'simple-timer-status-'.$vars['initial_status'];
  $vars['classes_array'][] = $id;
  $vars['classes_array'][] = 'simple-timer-category-'.$vars['timer_category'];
}

/**
 * Implements template_preprocess_simple_timer_timer().
 */
function template_preprocess_simple_timer_placeholder(&$vars) {
  global $user;

  $path = drupal_get_path('module', 'simple_timer');
  drupal_add_js($path.'/js/simple-timer.js');

  $element = $vars['simple_timer'];
  $vars['timer_uid'] = !empty($element['#timer_uid']) ? $element['#timer_uid'] : $user->uid;

  $id = 'timer-placeholder-'.$element['#timer_key'].'-'.$vars['timer_uid'];
  $vars['id'] = drupal_html_id($id);
  $vars['classes_array'][] = $id;
  $vars['timer_key'] = $element['#timer_key'];
  $vars['timer_category'] = $element['#category'];
  $vars['timer_behavior'] = !empty($element['#behavior']) ? $element['#behavior'] : 'show';
  $vars['classes_array'][] = 'simple-timer-behavior-'.$vars['timer_behavior'];
  $vars['classes_array'][] = 'simple-timer-category-'.$vars['timer_category'];

  if ($vars['timer_behavior'] == 'auto_alert') {
    // Look for running timers in the same category.
    $conflicting = entity_get_controller('simple_timer')->getConflicting((object) array(
      'category' => $vars['timer_category'],
      'uid' => $vars['timer_uid'],
    ));

    if (empty($conflicting)) {
      $vars['classes_array'][] = 'simple-timer-placeholder-auto-alert-no-conflicting';
    }
  }

  $vars['start_button_text'] = '&#9658';
}
