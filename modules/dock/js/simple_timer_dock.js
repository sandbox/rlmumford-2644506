(function($) {
  Drupal.behaviors.simple_timer_dock = {
    attach: function (context, settings) {
      $('.simple-timer-dock h2').click(function() {
        var $dock = $(this).closest('.simple-timer-dock');
        if ($dock.hasClass('expanded')) {
          $dock.find('.view').hide("slow", function() {
            $dock.removeClass('expanded').addClass('collapsed');
          });
        }
        else if ($dock.hasClass('collapsed')) {
          $dock.find('.view').show("slow", function() {
            $dock.removeClass('collapsed').addClass('expanded');
          });
        }
      });
    }
  };
}(jQuery))
