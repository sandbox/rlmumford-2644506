<?php
/**
 * @file
 * Contains Module Code.
 */

/**
 * Implements hook_init().
 */
function simple_timer_init() {
  drupal_add_js(array('simple_timer' => array('use_geoloc' => user_is_logged_in())), 'setting');
}

/**
 * Implements hook_entity_info().
 */
function simple_timer_entity_info() {
  $info = array();
  $info['simple_timer'] = array(
    'label' => t('Timer'),
    'entity class' => 'SimpleTimer',
    'controller class' => 'SimpleTimerController',
    'base table' => 'simple_timer',
    'entity keys' => array(
      'id' => 'id',
    ),
    'module' => 'simple_timer',
    'fieldable' => TRUE,
  );
  return $info;
}

/**
 * Implements hook_menu().
 */
function simple_timer_menu() {
  $items = array();
  $items['simple_timer/%simple_timer'] = array(
    'title' => t('Edit Timer'),
    'page callback' => 'simple_timer_page',
    'page arguments' => array(1),
    'access callback' => 'simple_timer_operation_access',
    'access arguments' => array(1, 'pause'),
    'theme callback' => 'simple_timer_operation_theme',
  );
  if (module_exists('ctools')) {
    $items['simple_timer/%simple_timer/%ctools_js'] = $items['simple_timer/%simple_timer'];
    $items['simple_timer/%simple_timer/%ctools_js']['page arguments'] = array(1,2);
  }
  $items['simple_timer_op/%simple_timer/%'] = array(
    'page callback' => 'simple_timer_operation_page',
    'page arguments' => array(1,2),
    'access callback' => 'simple_timer_operation_access',
    'access arguments' => array(1,2),
    'theme callback' => 'simple_timer_operation_theme',
    'delivery callback' => 'ajax_deliver',
  );
  $items['simple_timer_start/%/%'] = array(
    'page callback' => 'simple_timer_operation_page',
    'page arguments' => array(2, 'start', 1),
    'access callback' => 'simple_timer_operation_access',
    'access arguments' => array(2, 'start'),
    'theme callback' => 'simple_timer_operation_theme',
    'delivery callback' => 'ajax_deliver',
  );
  return $items;
}

/**
 * Implements hook_views_api().
 */
function simple_timer_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'simple_timer') . '/views',
  );
}

/**
 * Implements hook_modules_enabled().
 */
function simple_timer_modules_enabled($modules) {
  if (in_array('geolocation', $modules)) {
    module_load_include('inc', 'field', 'field.info');
    field_info_cache_clear();

    $field_names = array(
      'timer_start_geo' => t('Start Location'),
      'timer_end_geo' => t('End Location'),
    );
    foreach ($field_names as $field_name => $label) {
      if (!field_info_field($field_name)) {
        $field = array(
          'field_name' => $field_name,
          'type' => 'geolocation_latlng',
          'cardinality' => 1,
        );
        field_create_field($field);
      }
      if (!field_info_instance('simple_timer', $field_name, 'simple_timer')) {
        $instance = array(
          'field_name' => $field_name,
          'entity_type' => 'simple_timer',
          'bundle' => 'simple_timer',
          'label' => $label,
        );
        field_create_instance($instance);
      }
    }
  }
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function simple_timer_ctools_plugin_directory($owner, $plugin_type) {
  if (($owner == 'ctools' && $plugin_type == 'content_types')) {
    return "plugins/$plugin_type";
  }
}

/**
 * Load a simple timer.
 */
function simple_timer_load($timer_key, $uid = NULL) {
  // If the timer_key is numeric, assume the id was passed.
  if (is_numeric($timer_key)) {
    return entity_load_single('simple_timer', $timer_key);
  }

  global $user;
  if (empty($uid)) {
    $uid = $user->uid;
  }

  $q = new EntityFieldQuery();
  $q->entityCondition('entity_type', 'simple_timer');
  $q->propertyCondition('uid', $uid);
  $q->propertyCondition('timer_key', $timer_key);
  $result = $q->execute();

  if (empty($result['simple_timer'])) {
    return FALSE;
  }

  $timer_ids = array_keys($result['simple_timer']);
  return entity_load_single('simple_timer', reset($timer_ids));
}

/**
 * Access callback for simple timer operations.
 *
 * @todo: Implement entity access.
 */
function simple_timer_operation_access($timer, $op) {
  if ($op == 'start') {
    $uid = !empty($_GET['timer_uid']) ? $_GET['timer_uid'] : NULL;
    $timer = simple_timer_load($timer, $uid);

    // Only start a timer if one does not exist with the same timer_key.
    if (!$timer) {
      return TRUE;
    }

    return FALSE;
  }

  if (!in_array($op, array('stop', 'pause', 'unpause', 'delete'))) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Theme callback for simple timer operations.
 */
function simple_timer_operation_theme() {
  global $user;

  if (!empty($_GET['theme'])) {
    return $_GET['theme'];
  }

  if (!empty($_POST['ajax_page_state']['theme'])) {
    return $_POST['ajax_page_state']['theme'];
  }

  $theme = variable_get('simple_timer_operation_theme', '__admin');
  if ($theme == '__admin') {
    $theme = variable_get('admin_theme');
  }
  else if ($theme == '__default') {
    $theme = !empty($user->theme) && drupal_theme_access($user->theme) ? $user->theme : variable_get('theme_default', 'bartik');
  }

  return $theme;
}

/**
 * Page callback for simple timer operations.
 */
function simple_timer_operation_page($timer, $op, $category = NULL) {
  $commands = array();

  if ($op == 'start') {
    $uid = !empty($_GET['timer_uid']) ? $_GET['timer_uid'] : NULL;
    $timer = SimpleTimer::start($timer, $category, $uid);
    $render = array(
      '#theme' => 'simple_timer_timer',
      '#timer' => $timer,
    );
    $commands[] = ajax_command_replace('.timer-placeholder-'.$timer->timer_key.'-'.$uid, render($render));
  }
  else {
    $timer->$op();
    if (in_array($op, array('stop', 'delete'))) {
      $new_timer_key = $timer->timer_key;

      // If this is a stop then we need to generate a new key.
      if ($op == 'stop') {
        // Add Ajax command to remove the entity from the page.
        // Generate a new timer key.
        $key_bits = explode('--', $timer->timer_key);
        $new_timer_key = '';
        if (count($key_bits) < 2) {
          $new_timer_key = $timer->timer_key.'--0';
        }
        else {
          $delta = array_pop($key_bits);
          if (is_numeric($delta)) {
            array_push($key_bits, $delta + 1);
            $new_timer_key = implode('--', $key_bits);
          }
          else {
            $new_timer_key = $timer->timer_key.'--0';
          }
        }
      }
      $render = array(
        '#theme' => 'simple_timer_placeholder',
        '#timer_key' => $new_timer_key,
        '#timer_uid' => $timer->uid,
        '#category' => $timer->category,
      );
      $commands[] = ajax_command_replace('.timer-id-'.$timer->id, render($render));
    }
    else {
      // Refresh the timer.
      $render = array(
        '#theme' => 'simple_timer_timer',
        '#timer' => $timer,
      );
      $commands[] = ajax_command_replace('.timer-id-'.$timer->id, render($render));
    }
  }

  if (!empty($timer->pausedIds)) {
    foreach (entity_load('simple_timer', $timer->pausedIds) as $paused_timer) {
      $render = array(
        '#theme' => 'simple_timer_timer',
        '#timer' => $paused_timer,
      );
      $commands[] = ajax_command_replace('.timer-id-'.$paused_timer->id, render($render));
    }
  }

  drupal_alter('simple_timer_operation_commands', $commands, $timer, $op);
  return array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );
}

/**
 * Page callback for the simple timer page.
 */
function simple_timer_page($timer, $js = FALSE) {
  $form_id = 'simple_timer_form';
  $form_state = array(
    'ajax' => TRUE,
    'build_info' => array(
      'args' => array($timer),
    ),
  );

  // If we're running in AJAX, pass onto CTools.
  if ($js) {
    // This is in a modal, so let's do the modal magic.
    ctools_include('ajax');
    ctools_include('modal');

    $commands = ctools_modal_form_wrapper($form_id, $form_state);

    if (!empty($form_state['executed']) && empty($form_state['rebuild'])) {
      // The form has been executed, so let's redirect to the destination page.
      $commands = array();
      if (!empty($_GET['destination'])) {
        $commands[] = ctools_ajax_command_redirect($_GET['destination']);
      }
      elseif (!empty($form_state['redirect'])) {
        $commands[] = ctools_ajax_command_redirect($form_state['redirect']);
      }
      elseif (!empty($form_state['modal_dismiss'])) {
        $commands[] = ctools_modal_command_dismiss();
      }
      else {
        $commands[] = ctools_ajax_command_reload();
      }
    }

    print ajax_render($commands);
    ajax_footer();
    return;
  }
  // Otherwise build normally.
  else {
    // This is just a page, so we don't need to worry.
    return drupal_build_form($form_id, $form_state);
  }
}

/**
 * Form for editing timers.
 */
function simple_timer_form($form, &$form_state, $timer) {
  $form_state['timer'] = $timer;

  $form['timings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Timer Information'),
    '#weight' => -100,
  );
  $form['timings']['key'] = array(
    '#type' => 'item',
    '#title' => t('Key'),
    '#markup' => $timer->timer_key,
  );
  $form['timings']['user'] = array(
    '#type' => 'item',
    '#title' => t('User'),
    '#markup' => format_username(user_load($timer->uid)),
  );
  $cat_info = simple_timer_category_info($timer->category);
  $form['timings']['category'] = array(
    '#type' => 'item',
    '#title' => t('Category'),
    '#markup' => $cat_info['label'],
  );
  $form['timings']['status'] = array(
    '#type' => 'item',
    '#title' => t('Status'),
    '#markup' => $timer->status,
  );
  $form['timings']['start'] = array(
    '#type' => 'date_popup',
    '#title' => t('Start Time'),
    '#timepicker_options' => array(
      'showPeriod' => 1,
      'periodSeparator' => '',
      'amPmText' => array('am','pm'),
    ),
    '#default_value' => date('Y-m-d H:i', $timer->start),
  );
  $form['timings']['end'] = array(
    '#type' => 'date_popup',
    '#title' => t('End Time'),
    '#default_value' => date('Y-m-d H:i', $timer->end),
    '#timepicker_options' => array(
      'showPeriod' => 1,
      'periodSeparator' => '',
      'amPmText' => array('am','pm'),
    ),
    '#access' => !empty($timer->end),
  );

  field_attach_form('simple_timer', $timer, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 150,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate callback.
 */
function simple_timer_form_validate($form, &$form_state) {
  $timer = clone $form_state['timer'];
  field_attach_form_validate('simple_timer', $timer, $form, $form_state);

  if ($form_state['values']['start'] >= $form_state['values']['end']) {
    form_error($form['timings']['end'], t('A timer cannot stop before it starts.'));
  }
}

/**
 * Submit callback.
 */
function simple_timer_form_submit($form, &$form_state) {
  $timer = $form_state['timer'];
  field_attach_submit('simple_timer', $timer, $form, $form_state);

  $timer->start = DateTime::createFromFormat('Y-m-d H:i', $form_state['values']['start'])->getTimestamp();
  $timer->end = strtotime($form_state['values']['end']);

  $timer->save();
}

/**
 * Implements hook_theme().
 */
function simple_timer_theme() {
  $theme['simple_timer_timer'] = array(
    'render element' => 'simple_timer',
    'file' => 'simple_timer.theme.inc',
    'template' => 'simple-timer-timer',
  );
  $theme['simple_timer_placeholder'] = array(
    'render element' => 'simple_timer',
    'file' => 'simple_timer.theme.inc',
    'template' => 'simple-timer-placeholder',
  );
  return $theme;
}

/**
 * Implements hook_entity_view_alter().
 */
function simple_timer_entity_view_alter(&$build, $type) {
  if ($type != 'simple_timer') {
    return;
  }

  $build['timer'] = array(
    '#theme' => 'simple_timer_timer',
    '#timer' => $build['#entity'],
  );
}

/**
 * Get simple timer categories.
 */
function simple_timer_category_info($category = NULL) {
  $categories = &drupal_static(__FUNCTION__, NULL);
  if (!is_array($categories)) {
    $categories = module_invoke_all('simple_timer_category_info');
  }

  if (!is_null($category)) {
    return !empty($categories[$category]) ? $categories[$category] : FALSE;
  }
  else {
    return $categories;
  }
}

/**
 * Get the default simple time category.
 */
function simple_timer_simple_timer_category_info() {
  return array(
    'default' => array(
      'label' => t('Default'),
      'blocking' => TRUE,
    ),
  );
}
