<?php
/**
 * @file
 * Simple Time Placeholder Template.
 */
?>
<div class="<?php print $classes; ?>" id="<?php print $id; ?>" timer-category="<?php print $timer_category; ?>" timer-key="<?php print $timer_key; ?>" timer-uid="<?php print $timer_uid; ?>" timer-behavior="<?php print $timer_behavior ?>">
  <div class="controls">
  <button class="button timer-control timer-start" title="Start"><?php print $start_button_text; ?></button>
  </div>
</div>
