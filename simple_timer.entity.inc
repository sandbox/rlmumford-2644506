<?php
/**
 * @file
 * Contains SimpleTimer and SimpleTimerController.
 */

class SimpleTimer extends Entity {

  public static function start($timer_key, $category = 'default', $uid = NULL, $time = NULL) {
    return entity_get_controller('simple_timer')->start($timer_key, $category, $uid, $time);
  }

  public function pause($time = NULL) {
    entity_get_controller('simple_timer')->pause($this, $time);
  }

  public function unpause($time = NULL) {
    entity_get_controller('simple_timer')->unpause($this, $time);
  }

  public function stop($time = NULL) {
    entity_get_controller('simple_timer')->stop($this, $time);
  }

  public function duration() {
    // Get the duration of a timer in seconds.
    return entity_get_controller('simple_timer')->calculateDuration($this);
  }

  public function durationArray() {
    $date = new DateTime('@'.$this->duration(), new DateTimeZone('UTC'));
    return array(
      'days' => $date->format('z'),
      'hours' => $date->format('G'),
      'minutes' => $date->format('i'),
      'seconds' => $date->format('s'),
    );
  }
}

class SimpleTimerController extends EntityAPIController {

  public function create(array $values = array()) {
    $entity = parent::create($values);
    $this->invoke('create', $entity);
    return $entity;
  }

  public function start($timer_key, $category = 'default',  $uid = NULL, $time = NULL) {
    global $user;

    if (empty($uid)) {
      $uid = $user->uid;
    }

    if (empty($time)) {
      $time = REQUEST_TIME;
    }

    $entity = $this->create(array(
      'timer_key' => $timer_key,
      'category' => $category,
      'uid' => $uid,
      'start' => $time,
      'start_ip' => ip_address(),
      'status' => 'active',
    ));

    // Get the latitude and longitude.
    if (!empty($_GET['lat']) || !empty($_GET['lon'])) {
      $entity->timer_start_geo[LANGUAGE_NONE][0] = array(
        'lat' => $_GET['lat'],
        'lng' => $_GET['lon'],
      );
    }
    $this->save($entity);
    $this->invoke('start', $entity);

    $this->pauseConflicting($entity, $time);
    return $entity;
  }

  public function stop($entity, $time = NULL) {
    if (empty($time)) {
      $time = REQUEST_TIME;
    }

    $entity->end = $time;
    $entity->end_ip = ip_address();

    // Attempt to get the latitude and longitude.
    if (!empty($_GET['lat']) || !empty($_GET['lon'])) {
      $entity->timer_end_geo[LANGUAGE_NONE][0] = array(
        'lat' => $_GET['lat'],
        'lng' => $_GET['lon'],
      );
    }
    $entity->status = 'stopped';
    $this->save($entity);
    $this->invoke('stop', $entity);
  }

  public function pause($entity, $time = NULL) {
    if (empty($time)) {
      $time = REQUEST_TIME;
    }

    // Only pause if it is active.
    if ($entity->status == 'active') {
      $entity->status = 'paused';

      // Add a pause.
      db_insert('simple_timer_pause')
        ->fields(array(
          'id' => $entity->id,
          'start' => $time,
        ))
        ->execute();
      $entity->save();
      $entity->pause_time;
      $this->invoke('pause', $entity);
    }
  }

  public function unpause($entity, $time = NULL) {
    if (empty($time)) {
      $time = REQUEST_TIME;
    }

    if ($entity->status == 'paused') {
      $entity->status = 'active';

      db_update('simple_timer_pause')
        ->fields(array(
          'end' => $time,
        ))
        ->condition('id', $entity->id)
        ->condition('end', 0)
        ->execute();
      $entity->save();
      $entity->unpause_time = $time;
      $this->invoke('unpause', $entity);

      $this->pauseConflicting($entity, $time);
    }
  }

  public function pauseConflicting($entity, $time = NULL) {
    if (empty($time)) {
      $time = REQUEST_TIME;
    }

    foreach ($this->getConflicting($entity) as $timer) {
      $this->pause($timer, $time);
    }

    $entity->pausedIds = $ids;
  }

  public function getConflicting($entity) {
    $category_info = simple_timer_category_info();
    $blocking = !empty($category_info[$entity->category]['blocking']);
    if (variable_get('simple_timer_allow_multiple_running', FALSE) || !$blocking) {
      return array();
    }

    $blocking_cats = array();
    foreach ($category_info as $cat => $info) {
      if (!empty($info['blocking'])) {
        $blocking_cats[] = $cat;
      }
    }
    $query = db_select('simple_timer', 't')
      ->fields('t', array('id'))
      ->condition('uid', $entity->uid)
      ->condition('category', $blocking_cats)
      ->condition('status', 'active');
    if (!empty($entity->id)) {
      $query->condition('id', $entity->id, '<>');
    }
    $ids = $query->execute()->fetchCol();

    return $this->load($ids);
  }

  public function calculateDuration($entity) {
    $start = $entity->start;
    $end = !empty($entity->end) ? $entity->end : REQUEST_TIME;
    $duration = $end - $start;

    $pause_query = db_select('simple_timer_pause', 'p');
    $pause_query->addExpression('SUM(IF(p.end, p.end,'.REQUEST_TIME.') - p.start)', 'pause_duration');
    $pause_query->condition('p.id', $entity->id);
    $pause_duration = $pause_query->execute()->fetchField();

    return $duration - $pause_duration;
  }

  public function invoke($hook, $entity) {
    parent::invoke($hook, $entity);

    // After all delete hooks have run remove the pause records for this timer.
    if ($hook == 'delete') {
      db_delete('simple_timer_pause')
        ->condition('id', $entity->id)
        ->execute();
    }
  }
}
