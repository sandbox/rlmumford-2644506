/**
 * @file
 * Simple Timer Module JS.
 */

(function ($) {
  "use strict";

  Drupal.behaviors.simple_timer = {
    attach: function (context, settings) {
      if (Drupal.settings.simple_timer.use_geoloc && navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          Drupal.behaviors.simple_timer.geoloc = position.coords;
        });
      }

      $(".simple-timer-placeholder").once("simple-timer-placeholder", function() {
        $(this).find("button.timer-start").click(function() {
          var $placeholder = $(this).closest(".simple-timer-placeholder");
          var base = "/simple_timer_start/"+$placeholder.attr("timer-category")+"/"+$placeholder.attr("timer-key")+"?timer_uid="+$placeholder.attr("timer-uid");
          if (typeof Drupal.behaviors.simple_timer.geoloc !== "undefined") {
            base = base+"&lat="+Drupal.behaviors.simple_timer.geoloc.latitude+"&lon="+Drupal.behaviors.simple_timer.geoloc.longitude;
          }
          var ajaxSettings = {
            url: base,
            event: "startTimer",
            progress: { type: "throbber" }
          };
          Drupal.ajax[base] = new Drupal.ajax(base, this, ajaxSettings);
          $(this).trigger("startTimer");
        });

        let urlParams = new URLSearchParams(window.location.search);

        // Handle Initial Behaviours.
        var timer_behavior = urlParams.has("no-auto-timer") ? "placeholder" : $(this).attr("timer-behavior");
        var timer_el = this;
        var confirm_options = {
          title: "Start the timer?",
          content: "This will pause all other timers.",
          animation: "zoom",
          closeAnimation: "zoom",
          draggable: true,
          boxWidth: "25%",
          backgroundDismiss: function () {
            return "cancel";
          },
          buttons: {
            confirm: {
              text: "Yes",
              btnClass: "btn-green",
              keys: ["enter"],
              action: function() {
                $(timer_el).find("button.timer-start").click();
              }
            },
            cancel: {
              text: "No",
              btnClass: "btn-red",
              keys: ["esc"],
            }
          }
        };

        if (timer_behavior === "auto") {
          $(this).find("button.timer-start").click();
        }
        else if (timer_behavior === "alert") {
          if (typeof $.confirm === "function") {
            $.confirm(confirm_options);
          }
          else if (window.confirm("Start the timer? This will pause other timers.")) {
            $(this).find("button.timer-start").click();
          }
        }
        else if (timer_behavior === "auto_alert") {
          if ($(this).hasClass("simple-timer-placeholder-auto-alert-no-conflicting")) {
            $(this).find("button.timer-start").click();
          }
          else if (typeof $.confirm === "function") {
            $.confirm(confirm_options);
          }
          else if (window.confirm("Start the timer? This will pause other timers.")) {
            $(this).find("button.timer-start").click();
          }
        }
      });
      $(".simple-timer-timer").once("simple-timer-timer", function() {
        // Get the initial state of the timer.
        var state = "running";
        if ($(this).attr("status") == "stopped") {
          state = "stopped";
        }
        else if ($(this).attr("status") == "paused") {
          state = "paused";
        }

        // Set Controls.
        $(this).find("button.timer-pause").click(function() {
          $(this).closest(".simple-timer-timer").find(".timer").timer("pause");
          $(this).siblings("button.timer-resume").removeAttr("disabled").css("display", "");
          $(this).attr("disabled", "disabled").css("display", "none");
        });
        $(this).find("button.timer-resume").click(function() {
          $(this).closest(".simple-timer-timer").find(".timer").timer("resume");
          $(this).siblings("button.timer-pause").removeAttr("disabled").css("display", "");
          $(this).attr("disabled", "disabled").css("display", "none");
        });
        $(this).find("button.timer-stop").click(function() {
          var base = "/simple_timer_op/"+$(this).closest(".simple-timer-timer").attr("timer-id")+"/stop";
          if (typeof Drupal.behaviors.simple_timer.geoloc !== "undefined") {
            base = base+"?lat="+Drupal.behaviors.simple_timer.geoloc.latitude+"&lon="+Drupal.behaviors.simple_timer.geoloc.longitude;
          }
          if (typeof Drupal.ajax[base] === "undefined") {
            var ajaxSettings = {
              url: base,
              event: "stopTimer",
              progress: { type: "throbber" }
            }
            Drupal.ajax[base] = new Drupal.ajax(base, this, ajaxSettings);
          }
          $(this).trigger("stopTimer");

          $(this).closest(".simple-timer-timer").find(".timer").timer("remove");
          $(this).siblings("button").attr("disabled", "disabled").css("display", "none");
          $(this).attr("disabled", "disabled").css("display", "none");
        });
        $(this).find("button.timer-cancel").click(function() {
          var base = "/simple_timer_op/"+$(this).closest(".simple-timer-timer").attr("timer-id")+"/delete";
          if (typeof Drupal.ajax[base] === "undefined") {
            var ajaxSettings = {
              url: base,
              event: "deleteTimer",
              progress: { type: "throbber" }
            }
            Drupal.ajax[base] = new Drupal.ajax(base, this, ajaxSettings);
          }

          $(this).trigger("deleteTimer");
          $(this).closest(".simple-timer-timer").find(".timer").timer("remove");
          $(this).siblings("button").attr("disabled", "disabled").css("display", "none");
          $(this).attr("disabled", "disabled").css("display", "none");
        });

        // Start the actual timer.
        $(this).find(".timer").timer({
          seconds: $(this).attr("init"),
          state: state,
          format: "%hh %Mm %Ss",
          pauseTimer: function() {
            var base = "/simple_timer_op/"+$(this.element).parent().attr("timer-id")+"/pause";
            if (typeof Drupal.ajax[base] === "undefined") {
              var ajaxSettings = {
                url: base,
                event: "pauseTimer",
                progress: { type: "throbber" }
              }
              Drupal.ajax[base] = new Drupal.ajax(base, this.element, ajaxSettings);
            }
            $(this.element).trigger("pauseTimer");
          },
          resumeTimer: function() {
            var base = "/simple_timer_op/"+$(this.element).parent().attr("timer-id")+"/unpause";
            if (typeof Drupal.ajax[base] === "undefined") {
              var ajaxSettings = {
                url: base,
                event: "unpauseTimer",
                progress: { type: "throbber" }
              }
              Drupal.ajax[base] = new Drupal.ajax(base, this.element, ajaxSettings);
            }
            $(this.element).trigger("unpauseTimer");
          }
        });

        // Initialise button state.
        if (state != "running") {
          $(this).find("button.timer-pause").attr("disabled", "disabled").css("display", "none");
        }
        if (state != "paused") {
          $(this).find("button.timer-resume").attr("disabled", "disabled").css("display", "none");
        }
        if (state == "stopped") {
          $(this).find("button").attr("disabled", "disabled").css("display", "none");
        }

        var timer_el = this;
        if ($(this).hasClass("simple-timer-timer-show-unpause-alert")) {
          if (typeof $.confirm === "function") {
            $.confirm({
              title: "Resume the timer?",
              content: "This will pause all other timers.",
              animation: "zoom",
              closeAnimation: "zoom",
              draggable: true,
              boxWidth: "25%",
              buttons: {
                confirm: {
                  text: "Yes",
                  btnClass: "btn-green",
                  keys: ["enter"],
                  action: function() {
                    $(timer_el).find("button.timer-resume").click();
                  }
                },
                cancel: {
                  text: "No",
                  btnClass: "btn-red",
                  keys: ["esc"],
                }
              }
            });
          }
          else if (window.confirm("Start the timer? This will pause other timers.")) {
            $(this).find("button.timer-resume").click();
          }
        }
      });
    }
  }
}(jQuery))
