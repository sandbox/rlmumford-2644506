<?php
/**
 * @file
 * Simple Timer Timer Template.
 */
?>
<div class="<?php print $classes; ?>" id="<?php print $id; ?>" init="<?php print $initial_seconds; ?>" timer-category="<?php print $timer_category; ?>" timer-id="<?php print $timer_id; ?>" timer-key="<?php print $timer_key; ?>" status="<?php print $initial_status; ?>">
  <div class="timer"></div>
  <div class="controls">
  <?php if ($show_stop) { ?><button class="button timer-control timer-stop" title="Stop"><?php print $stop_button_text; ?></button><?php } ?>
  <?php if ($show_pause) { ?><button class="button timer-control timer-pause" title="Pause"><?php print $pause_button_text; ?></button><?php } ?>
  <?php if ($show_resume) { ?><button class="button timer-control timer-resume" title="Resume"><?php print $resume_button_text; ?></button><?php } ?>
  <?php if ($show_cancel) { ?><button class="button timer-control timer-cancel" title="Cancel"><?php print $cancel_button_text; ?></button><?php } ?>
  </div>
</div>
