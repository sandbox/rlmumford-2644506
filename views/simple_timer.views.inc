<?php
/**
 * @file
 * Views Integration with Simple Timer.
 */

/**
 * Implements hook_views_data_alter().
 */
function simple_timer_views_data_alter(&$data) {
  $data['views']['simple_timer_timer'] = array(
    'title' => t('Start or Show Timer'),
    'help' => t('Show options to either start a new timer or show an existing timer.'),
    'field' => array(
      'handler' => 'simple_timer_views_handler_field_timer',
    ),
  );
}
