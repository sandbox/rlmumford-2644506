<?php
/**
 * @file
 * Contains simple_timer_views_handler_field_timer.
 */

class simple_timer_views_handler_field_timer extends views_handler_field {

  function option_definition() {
    $options = parent::option_definition();
    $options['timer_key'] = array('default' => '');
    $options['category'] = array('default' => 'default');
    $options['uid'] = array('default' => '');
    $options['show_stopped'] = array('default' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['timer_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Timer Key'),
      '#description' => t('The key of the timer to display/start. If a stopped timer already exists with this key and "Show Stopped Timers" is left unchecked a new key will be generated by appending "--x" to the end of the key where x is an integer. It is therefore best to avoid double hyphens ("--") in your timer keys.'),
      '#default_value' => $this->options['timer_key'],
    );

    $cat_options = array();
    foreach (simple_timer_category_info() as $category => $info) {
      $cat_options[$category] = $info['label'];
    }
    $form['category'] = array(
      '#type' => 'select',
      '#title' => t('Category'),
      '#options' => $cat_options,
      '#default_value' => $conf['category'],
    );

    $form['uid'] = array(
      '#type' => 'textfield',
      '#title' => t('User ID'),
      '#description' => t('The user id of the user to fetch the timer for.'),
      '#default_value' => $this->options['uid'],
    );

    $form['show_stopped'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show Stopped Timers'),
      '#description' => t('Show Timers that have stopped. If left unchecked and a stopped timer is found then a new timer key will be generated.'),
      '#default_value' => $this->options['show_stopped'],
    );
    parent::options_form($form, $form_state);
  }

  function render($values) {
    global $user;

    $tokens = $this->get_render_tokens(array());
    $timer_key = strtr($this->options['timer_key'], $tokens);
    $uid = strtr($this->options['uid'], $tokens);
    if (!is_numeric($uid)) {
      $uid = $user->uid;
    }

    $timer = simple_timer_load($timer_key, $uid);
    if ($timer && $timer->status == 'stopped' && !$this->options['show_stopped']) {
      // Look for a timer with a relevant key that is not stopped.
      $base_query = db_select('simple_timer', 't')
        ->condition('timer_key', db_like($timer_key.'--').'%', 'LIKE')
        ->condition('uid', $uid);

      $query = clone $base_query;
      $id = $query->condition('status', 'stopped', '<>')
        ->orderBy('timer_key', 'ASC')
        ->fields('t', array('id'))
        ->execute()
        ->fetchField();

      // If a non stopped timer is found with a derivative key then we show
      // that timer instead.
      if (!empty($id) && ($latest_timer = entity_load_single('simple_timer', $id))) {
        $timer = $latest_timer;
      }
      // Otherwise we work out what the next derivative key would be.
      else {
        $timer = FALSE;
        $new_timer_key = $base_query->fields('t', array('timer_key'))
          ->orderBy('CHAR_LENGTH(timer_key)', 'DESC')
          ->orderBy('timer_key', 'DESC')
          ->execute()
          ->fetchField();
        if (empty($new_timer_key)) {
          $timer_key .= '--0';
        }
        else {
          $key_bits = explode('--', $new_timer_key);
          $extra = array_pop($key_bits);
          if (is_numeric($extra)) {
            array_push($key_bits, $extra + 1);
          }
          else {
            array_push($key_bits, $extra . '--0');
          }
          $timer_key = implode('--', $key_bits);
        }
      }
    }

    if (!empty($timer)) {
      $render = array(
        '#theme' => 'simple_timer_timer',
        '#timer' => $timer,
      );
    }
    else {
      $render = array(
        '#theme' => 'simple_timer_placeholder',
        '#timer_key' => $timer_key,
        '#category' => $this->options['category'],
        '#uid' => $uid,
      );
    }

    return render($render);
  }

  function query() {}
}
